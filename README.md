#How to for ELK stack

The purpose of this training is to introduce you the ELK stack and what it allows you to do.

We are not going to go through all the details of Docker or Docker compose,
we supposed that you already have some knowledge about those topics. Those tools were mainly chosen to simplify our work.
We suppose that you already have an idea also on what is a NoSQL database and their advantages.

We’ll mainly focus on the ELK stack and its benefits.

In this repository is provided the tools and files that will allow you to launch easily all the stack,
for training purpose the files content very limited elements so you can modify them yourself.

##Introduction:

In the world of web applications and micro services, the logs are the first source of information for
us as a developers. When you developing an app it is easy to read the logs since they are already
in your IDE or your console.

But what happens when you deploy that application on cloud servers? Specially if you have different
componant on different servers?

The answer is simple, without a logging software that gattered all your logs, you have no other
choice that to ssh into all those servers and look at the logs until you find the source of the
problem.

That's why the ELK stack is for. It allows you to gatter all your application logs in a single
database and read/access it from one single screen.

##Description

With that beign said, first I am going to explain what is an ELK stack.
An ELK stack is a combination of the 3 following tools:

**Filebeat** Is a software that allows you to read files and send their contents to different
locations. More info on https://www.elastic.co/products/beats/filebeat

**ElasticSearch** Is a standard NoSQl database.
More info on https://www.elastic.co/

**Logstash** Is an pipeline tool allowing you to aggregate and parse your logs and send them to your favorite stash.
It allows you to input many format of logs and then modify the output.
More info https://www.elastic.co/products/logstash

**Kibana** Is an interface that allows you visualize your data, configure a good amount of dashboard for data visualization
including charts, pies and others.

More info https://www.elastic.co/products/kibana

##Architecture
The global architecture of what we are going to achieve here is represented by the schema below:

![architecture](images/elk_archi.png)

##Content of the repository
Now we are going to detail a little bit  what is in this repository.

**.env** In this we set up our Version of ELK.
You can set it directly in the docker-compose file or as environment variable.

**docker-compose.yml** In this we are defining what our containers will contain and defining the rules between them.
For exemple if you take the first set of rules concerning elasticsearch we define the build context and arguments,
the volumes, the ports etc...

####Configuration

#####Filebeat
To configure your filebeat, usually we use a file named **filebeat.yml**. This file is to be placed by default in
`usr/share/filebeat/`. In this repository the location is `filebeat/config/` and the content of this file
can look like this:

```
filebeat.inputs:
- type: log
  paths:
    - /var/log/*.log

output.logstash:
  enabled: true
  hosts: ["logstash:5044"]
```

This simply indicates to filebeat the path for your log files and the ouput. Filebeat has few input/output plugins.
To know more about those please refear to the links bellow:

 - https://www.elastic.co/guide/en/beats/filebeat/master/configuration-filebeat-options.html
 - https://www.elastic.co/guide/en/beats/filebeat/current/configuring-output.html

#####Logstash
To configure logstash, we use to differents files. The first one is `logstash.yml` which is used to configure the software itself.
you can find this one under `logstash/config/`. The second is the logstash pipeline configuration, this is the file we use
to indicates the inputs and ouputs plugins use and to apply the filters. In this repository, you can find this file under
`logstash/pipeline` and the content looks like this:

```
input {
	tcp {
		port => 9600
	}
#	beats {
#    	        port => 5044
#		add_field => {"new_field" => "brand new"}
#	}
}


#filter {
#  mutate {
#      remove_field => [ "host" ]
#      add_field => {"application_name" => "your_application_name"}
#  }
#}


## Add your filters / logstash plugins configuration here

output {
	elasticsearch {
		hosts => "elasticsearch:9200"
	}
}
```
In this file we indiciate two types of input, ___tcp___ and ___beats___ and one type of output which is
___elasticsearch___. You can add as many inputs as you want as long as the ports are open.

#####ElasticSearch
To configure your elasticsearch, use the file under `elasticsearch/config`. For our purpose, the configuration
we choose is the following:
```
--
## Default Elasticsearch configuration from elasticsearch-docker.
## from https://github.com/elastic/elasticsearch-docker/blob/master/build/elasticsearch/elasticsearch.yml
#
cluster.name: "docker-cluster"
network.host: 0.0.0.0

# minimum_master_nodes need to be explicitly set when bound on a public IP
# set to 1 to allow single node clusters
# Details: https://github.com/elastic/elasticsearch/pull/17288
discovery.zen.minimum_master_nodes: 1

## Use single node discovery in order to disable production mode and avoid bootstrap checks
## see https://www.elastic.co/guide/en/elasticsearch/reference/current/bootstrap-checks.html
#
discovery.type: single-node
```

#####Kibana
The configuration file of your kibana is under `kibana/config` and contains the following:

```
---
## Default Kibana configuration from kibana-docker.
## from https://github.com/elastic/kibana-docker/blob/master/build/kibana/config/kibana.yml
#
server.name: kibana
server.host: "0"
elasticsearch.url: http://elasticsearch:9200
```

Here we just give a server name and the location of the ___ElasticSearch___ server.

Something that might be good to understand is we pass the configurations of those tools by mapping the respective
repositoies to a location in the containers. That is the reason why we mapp the volumes.

So basically when we want to change the configuration of one of those softwares, we change in his
configuration file in this repository then restart the container.

In each repository you will find a Dockerfile containing the line to build the image.

##Let's Begin

Alright, after all those beautiful introductions we can now touch our keyboard.
So please checkout the project using git clone git@bitbucket.org:AliouneBKONE/docker-elk-stack.git

Make sure that you have docker and docker-compose installed on your computer. If you do not please refferer
to those links to install them

 - **Docker** https://docs.docker.com/docker-for-mac/install/
 - **Docker-compose** https://github.com/Yelp/docker-compose/blob/master/docs/install.md , https://docs.docker.com/compose/install/

Once you clone the project, in the main directory, do the following command:

`$ docker-compose up -d`

This command will build and start running three containers connected to each other. One containing
an ElasticSearch database, the second containing a Logstash server, and the third containing Kibana.

If you everything worked fine you should be able to your containers just by using
`docker ps`
You will see that the naiming is very simple and clear so you can regognize which one is running.

you can access kibana with this following url => `http://localhost:5601`
Check that your elasticsearch is up with the following command => `$ curl http://localhost:9200`
You should receive an answer looking like this:

```
{
  "name" : "HJFrx0j",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "pEB8Fb_WTq-uPuEVELvYRQ",
  "version" : {
    "number" : "6.4.2",
    "build_flavor" : "oss",
    "build_type" : "tar",
    "build_hash" : "04711c2",
    "build_date" : "2018-09-26T13:34:09.098244Z",
    "build_snapshot" : false,
    "lucene_version" : "7.4.0",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
```
And now let's test our setup.

Send some random logs with following command to logstash:

`$ curl -d '{"nike": "just do it"}' -XPOST http://localhost:9600 -v --max-time 1`

Then go to your kibana interface. If this is your first time you will have to create an index pattern.

An index pattern is ....

To Create your index pattern in Kibana, go to the interface then search for your project's name.

![kibana_index](images/kibana_create_index_pattern.png)

To see the list of all the index patterns in your elastic search, go the Dev Tools => Console in
Kibana and execute this command `GET _cat/indices`.

Once you have created your index pattern to the **Discover** tab, you should see the logs appearing
on your screen. You can choose to visualize the by field (check the left side to see the options).

![kibana_discover](images/kibana_discover.png)

##Now let's focus on the funny part

Logstash. If you remember, in the repository there is a very minimum confguration of logstash that I
said that we are going to make it evoluate ourslef.

As you saw you directly send your logs to Logstash through tcp requests. But, with many applications,
your are actually all placed in a file, and you can send this file to Logstash with softwares like
Filebeat. That is the case we are going to implement together.

The goal here is to setup a Filebeat that will read your file content and send it to the logstash.
You can directly sned your logs through Filebeat to your ElasticSearch ofcourse. The point of using
Logstash it's that it allows you to modify the format of your logs and add some fields, for exemple
your application name. Allowing you to, even with many applications logging in the same ElasticSearch,
to just search or create a pattern by application name and that's it.

Now we are going to run a Filebeat in a docker container, to make it parse your log file and send it
to your ELK stack.

Uncomment the Filebeat block in the docker-compose.yml file, it should be like this:

```
# filebeat:
  #   build:
  #     context: filebeat/
  #   volumes:
  #     - ./filebeat/config/filebeat.yml:/usr/share/filebeat/config/filebeat.yml:ro
  #     - ./filebeat/config/filebeat.yml:/usr/share/filebeat/filebeat.yml:ro
  #     - ./logs:/var/log:ro
  #   ports:
  #     - "9000:9000"
  #   networks:
  #     - elk
  #   depends_on:
  #     - elasticsearch
```

As you can see in the Filebeat configuration file, we are loading our log file into the container and
just sending it through Logstash and as a result we can see those logs in our Kibana interface.

Now for fun we are going to modify those logs and add some fields in Logstash.
Modify the Logstash configuration file by uncommenting the filter tag in `logstash/pipeline/logstash.conf`:

```
filter {
  mutate {
      remove_field => [ "host" ]
      add_field => {"application_name" => "your_application_name"}
  }
}
```

Note: this configuration up will only affects the ___tcp___ inputs.
We remove the field ___host___ because it is a know bug in ELK
for ___tcp___ inputs.

Your you can directly add fields in the input area like this:

```
input {
        ...
	beats {
    	        port => 5044
		add_field => {"new_field" => "brand new"}
        }
}
```

Here we are adding a field name `new_field` with a value `brand new` to our ___beats___ inputs.
You can do the same on the ___tcp___ inputs.

The point of this is when you have multiple entries, you can still add your own fields to allow
you to filter them by entry point so you can focus on all the logs of your different applications,
or just on one specific.

Once you modified your configuration file, restart your logstash container and go check your log
in Kibana.

You should see the field added like this :

![kibana_fields](images/kibana_fields.png)

Note:

If you do not see your logs think about changing the range of time in Kibana in the upper right:

![kibana_clock](images/kibana_clock.png)


Once you at this point, you can filter by fields in kibana so you can just filter by your application
field and you will only see the logs you intersted in.

###Live Reload

Now let's modify the log file and see what happen. Open it with your favorite editor. I'll use emacs

`$ emacs -nw logs/toto.log`

And we are going to add the following line (you can add any thing you want)

```
ERROR  Exception in thread "main" java.lang.NullPointerException
        at com.example.myproject.Book.getTitle(Book.java:16)
        at com.example.myproject.Author.getBookTitles(Author.java:25)
        at com.example.myproject.Bootstrap.main(Bootstrap.java:14)
```

Save the file, then go refresh your kibana, you will see the line appears. So basically anytime your log
file gets filed, it will appear in your kibana.

![kibana_reload](images/live_reload.png)

Note:
The setup I just detailled is usefull if you are working with legacy applications.
If you are starting an application from scratch or have easy access to the source code, you can just
modified the logs to add your field and load directly your logs into ElasticSearch just by using logstash.
Filebeat is relevant when it comes to the fact where you have many applications on different servers.

To make logstash load your logs from your files, just add the file input your Logstash pipeline configuration:

```
input {
  file {
    path => "<path_to_your_file>"
    start_position => "beginning"
  }
}
```

###Plus
I would like ot take the time here to talk a little bit about the **grok filter plugin** of Logstash.
To use the **grok** filter, you basecally add this to your filter configuration:

```
grok {
    match => { "message" => "%{IP:client} %{WORD:method} %{URIPATHPARAM:request} %{NUMBER:bytes} %{NUMBER:duration}" }
  }
```
What it allows you to do is to use a regexp or a pattern to identify different field and tags in your messages and then attach them
to a a new field. It makes you logs more readable. The filter bellow will put every line that will match the pattern into a
field name message. So you can just have specific field for everything pattern you need to identify.
Also the is already a lot of patterns that has been done by the community to make the use of grok easier. For more informations
about those patterns please visit: https://github.com/elastic/logstash/blob/v1.4.2/patterns/grok-patterns

###Advice
As said in the begining of this content, docker is used only as a facilitator. If you are going to use it as it is,
it is **highly recommended** to implement a **physical storage system** for at least **ElasticSearch**, so you will not
lost your data if somehow you loose the container and have to recreate it.

For safety purpose, it is recommended to use ElasticSearch as a service and not in a container.

###Conclusion
This is it, now you have a setup enabling you to just load your logs automaticly.
This is really a simple setup to show you how practical this tool can be. For more information you should read the
official documentation.